\documentclass[12pt,a4paper,oneside,hidelinks]{beamer}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{lmodern}
\usepackage{csquotes}
\usepackage{listings}
\usepackage[font=scriptsize]{caption}
\usepackage[numbers]{natbib}
\usepackage{textcomp}

\usetheme[progressbar=frametitle,sectionpage=none,numbering=fraction]{metropolis}
\captionsetup[figure]{labelformat=empty}

\title{Ferroelectric RAM (FRAM)}
\subtitle{\small{Processor Design}}
\author{Sergi Granell}
\institute{Universitat Politècnica de Catalunya}
\date{December, 2018}


\begin{document}

\begin{frame}[plain,noframenumbering]
	\titlepage
\end{frame}

\section{FerroElectric RAM}

\begin{frame}{FRAM}
\begin{itemize}
\item Random-access memory (r/w any address)
\item Non-volatile (similar functionality as Flash memory)
\item Very similar construction to DRAM (1T-1C)
\item Uses a ferroelectric layer instead of a dielectric layer 
\end{itemize}
\end{frame}

\begin{frame}{FRAM vs other memory technologies}
\begin{itemize}
\item Advantages
	\begin{itemize}
	\item No charge to be displaced $\Rightarrow$ much more energy efficient
	\item \enquote{Trillions} read/write cycles
	\item More than 10 years at +85\textdegree{}C
	\end{itemize}
\item Disadvantages
	\begin{itemize}
	\item Lower storage density
	\item Less total capacity
	\item More expensive
	\end{itemize}
\end{itemize}
\begin{figure}[h]
\centering
\includegraphics[width=\textwidth]{figures/fram_vs.png}
\caption{\citet{fujitsu2018}}
\end{figure}	
% https://www.radio-electronics.com/info/data/semicond/memory/fram-ferroelectric-random-access-memory-basics-tutorial.php
\end{frame}

\begin{frame}{Capacitor, dielectric}
\begin{itemize}
\item $C = \frac{Q}{V} = \frac{\varepsilon A}{d}$
\item $\varepsilon =$ permittivity of dielectric ($\varepsilon_0$ in vacuum)
\end{itemize}
\begin{figure}[h]
\centering
\includegraphics[width=0.6\textwidth]{figures/parallel_plate_capacitor.png}
\caption{\citet{wikipedia}}
\end{figure}	
\end{frame}

\begin{frame}{Dielectric polarization}
\begin{itemize}
\item $E$: Applied electric field  ($V/m$)
\item $P$: Polarization ($C/m^2$)
\end{itemize}
    \begin{figure}[ht]
        \begin{minipage}[b]{0.45\linewidth}
            \centering
             \caption{Dielectric polarization}
            \includegraphics[width=\textwidth]{figures/dielectric_polarization.png}
        \end{minipage}
        \hspace{0.5cm}
        \begin{minipage}[b]{0.45\linewidth}
            \centering
            \caption{Paraelectric polarization}
            \includegraphics[width=\textwidth]{figures/paraelectric_polarization.png}
        \end{minipage}
        \caption{\citet{wikipedia}}
    \end{figure}
\end{frame}

\begin{frame}{Capacitor operation}
\begin{itemize}
\item $C = \frac{Q}{V} = \frac{\varepsilon A}{d}$
\end{itemize}
\begin{figure}[h]
\centering
\includegraphics[width=0.475\textwidth]{figures/capacitor.png}
\caption{\citet{wikipedia}}
\end{figure}	
\end{frame}

\begin{frame}{Ferroelectric materials}
\begin{itemize}
\item Polar even when no $\vec{E}$ is applied (spontaneous polarization)
\item Polarization can be reversed by an external $\vec{E}$
\item Analogy to ferromagnetism: permanent magnetic moment
\item Only ferroelectric below Curie temperature ($T_C$)
\item Force/temperature change lattice $\Rightarrow$ change polarization
\item Not affected by magnetic fields or radiation
\end{itemize}
\end{frame}

\begin{frame}{Ferroelectric Hysteresis Loop}
\begin{itemize}
\item $P_S$: Saturation polarization
\item $P_R$: Remanent polarization
\item $E_C$: Coercive electric field
\end{itemize}
\begin{figure}[h]
\centering
\includegraphics[width=0.85\textwidth]{figures/ferroelectric_hysteresis.png}
\caption{\citet{globalsino:ferroelectric}}
\end{figure}
\end{frame}

\begin{frame}{Perovskite structure}
\begin{itemize}
\item General chemical formula $ABX_3$, cubic structure
	\begin{itemize}
	\item $A$: Larger metal cation ($+$ ion)
	\item $B$: Smaller metal cation ($+$ ion)
	\item $X$: Usually oxygen anion ($-$ ion)
	\end{itemize}
\item Magnetoresistance, ferroelectricity, superconductivity, ...
\end{itemize}
\begin{figure}[h]
\centering
\includegraphics[width=0.4\textwidth]{figures/perovskite.jpg}
\caption{\citet{wikipedia}}
\end{figure}
\end{frame}

\begin{frame}{Lead Zirconate Titanate [$Pb(ZrTi)O_3$] (PZT)}
\begin{itemize}
\item Ceramic perovskite material
\item Physical shape changes when an $\vec{E}$ is applied (actuator applications)
\item Voltage changes when changing temperature (heat sensor)
\item Spontaneous electric polarization (electric dipole), reversible by $\vec{E}$
\end{itemize}
\end{frame}

\begin{frame}{PZT structure}
\begin{itemize}
\item $T > T_c$ $\Rightarrow$ paraelectric
\item $T < T_c$ $\Rightarrow$ ferroelectric
\end{itemize}
    \begin{figure}[ht]
        \begin{minipage}[b]{0.49\linewidth}
            \centering
            \includegraphics[width=\textwidth]{figures/PZT.png}
            \caption{\citet{wikipedia}}
        \end{minipage}
        %\hspace{0.1cm}
        \begin{minipage}[b]{0.49\linewidth}
            \centering
            \includegraphics[width=\textwidth]{figures/pzt_operation.png}
            \caption{\citet{fujitsu2018}}
        \end{minipage}
    \end{figure}
\end{frame}

\begin{frame}{FRAM cell}
    \begin{figure}[ht]
        \begin{minipage}[b]{0.49\linewidth}
            \centering
			\includegraphics[width=\textwidth]{figures/fram-cell.png}
			\caption{\citet{electronicsnotes:fram}}
        \end{minipage}
        %\hspace{0.1cm}
        \begin{minipage}[b]{0.49\linewidth}
            \centering
            \includegraphics[width=\textwidth]{figures/fram_cell_mos.png}
            \caption{\citet{wikipedia}}
        \end{minipage}
    \end{figure}
\end{frame}

\begin{frame}{FRAM write}
\begin{itemize}
\item Similar to DRAM write
\item Apply $\vec{E}$ across ferroelectric layer $\Rightarrow$ forces polarization
\end{itemize}

\begin{minipage}{0.49\linewidth}
Write procedure:
\begin{itemize}
\item Write $X$: \small{$V_{WL} = V_{DD}$ $V_{BL} = V_{DD}, V_{PL} = 0$}
\item Write $!X$: \small{$V_{WL} = V_{DD}$ $V_{BL} = 0, V_{PL} = V_{DD}$}
\end{itemize}
\end{minipage}
\begin{minipage}{0.49\linewidth}
	\centering
	\begin{figure}[ht]
	\includegraphics[width=\textwidth]{figures/fram-cell.png}
	\caption{\citet{electronicsnotes:fram}}
	\end{figure}
\end{minipage}

\end{frame}

\begin{frame}{FRAM read}
\begin{itemize}
\item Different than DRAM read, but also using sense amplifiers
\item Force polarization switch $\Rightarrow$ destructive
\end{itemize}

\begin{minipage}{0.49\linewidth}
Read procedure:
\begin{enumerate}
\item \alert{A}: \small{$V_{WL}, V_{BL}, V_{PL} = 0$}
\item \alert{B}: \small{$V_{WL}, V_{PL} = V_{DD}$}
\item If polarity switches $\Rightarrow$ \alert{C}
	\begin{itemize}
	\item $V_{BL} = \frac{C_s}{C_{BL}} V_{DD}$
	\end{itemize}
\end{enumerate}
\end{minipage}
\begin{minipage}{0.49\linewidth}
	\centering
	\begin{figure}[ht]
	\includegraphics[width=0.75\textwidth]{figures/fram_read.png}
	\caption{\citet{electronicsnotes:fram}}
	\end{figure}
\end{minipage}
\end{frame}

\begin{frame}{Commercial products}
\begin{figure}[h]
\centering
\includegraphics[width=0.80\textwidth]{figures/fujitsu_products.png}
\caption{\citet{fujitsu2018}}
\end{figure}	
\end{frame}

\begin{frame}{Conclusions}
\begin{itemize}
\item[$\uparrow$] Non-volatile
\item[$\uparrow$] Very energy efficient
\item[$\uparrow$] High speed access
\item[$\uparrow$] High endurance (trillions r/w cycles)
\item[$\uparrow$] Not affected by magnetic fields or radiation
\item[$\uparrow$] Almost unlimited lifetime (>10 years)
\end{itemize}
\begin{itemize}
\item[$\downarrow$] Small capacity
\item[$\downarrow$] More expensive
\end{itemize}
$\Rightarrow$ Suited for embedded systems on harsh environments?
\end{frame}

\appendix

\begin{frame}[plain,noframenumbering]
	\centering \Large
	\emph{Thanks for your attention!}
	\vskip10pt
	Questions?
\end{frame}

\begin{frame}[noframenumbering]{References}
\bibliographystyle{unsrtnat}
\bibliography{bibliography}
\end{frame}

\end{document}
