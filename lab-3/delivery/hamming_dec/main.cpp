#include <iostream>
#include <cstdio>
#include <cstdint>
#include <verilated.h>
#include "Vhamming_dec.h"

#define ARRAY_SIZE(x) (sizeof(x) / (sizeof *(x)))

/*
 * Hamming:
 * Given m parity bits:
 *   * 2^m - m - 1 data bits
 *   * 2^m - 1 total bits
 */
#define HAMMING_NUM_DATA ((1 << HAMMING_NUM_PARITY) - HAMMING_NUM_PARITY - 1)
#define HAMMING_NUM_TOTAL ((1 << HAMMING_NUM_PARITY) - 1)

static unsigned int binarray_to_uint(unsigned int din[], int size)
{
	unsigned int n = 0;

	for (int i = 0; i < size; i++)
		n |= din[i] << i;

	return n;
}

static void uint_to_binarray(unsigned int n, unsigned int din[], unsigned int size)
{
	for (unsigned int i = 0; i < size; i++)
		din[i] = (n >> i) & 1;
}

static void hamming_enc(const unsigned int din[HAMMING_NUM_DATA], unsigned int parity[HAMMING_NUM_PARITY])
{
	for (unsigned int p = 0; p < HAMMING_NUM_PARITY; p++) {
		unsigned int din_bit = 0;
		parity[p] = 0;
		for (unsigned int i = 1; i <= HAMMING_NUM_TOTAL; i++) {
			if (i != (i & ~(i - 1))) { /* is din bit */
				din_bit++;
				if (i & (1 << p))
					parity[p] ^= din[din_bit - 1];
			}
		}
	}
}

int main(int argc, char *argv[])
{
	srand(time(NULL));
	Verilated::commandArgs(argc, argv);

	Vhamming_dec *top = new Vhamming_dec;

	unsigned int din[HAMMING_NUM_DATA];
	unsigned int dout[HAMMING_NUM_DATA];
	unsigned int parity[HAMMING_NUM_PARITY];

	for (unsigned long long i = 0; i < 1ULL << HAMMING_NUM_DATA; i++) {
		/* C++ */
		uint_to_binarray(i, din, ARRAY_SIZE(din));
		hamming_enc(din, parity);
		unsigned int cpp_din = binarray_to_uint(din, ARRAY_SIZE(din));
		unsigned int cpp_parity = binarray_to_uint(parity, ARRAY_SIZE(parity));

		/* Flip a random data bit */
		unsigned int flipped = i ^ (1 << (rand() % HAMMING_NUM_DATA));

		/* SystemVerilog */
		top->din = flipped;
		top->parity = cpp_parity;
		top->eval();
		unsigned int sv_dout = top->dout;
		unsigned int sv_syndrome = top->hamming_dec__DOT__syndrome;
		unsigned int sv_data_error = top->data_error;

		if (cpp_din != sv_dout) {
			printf("FAILED:\n");
			printf("Orig:     0x%02X\n", cpp_din);
			printf("SV: Din:  0x%02X, parity: 0x%01X\n", flipped, cpp_parity);
			printf("SV: Dout: 0x%02X (syndrome: 0x%02X, data error: %d)\n", sv_dout, sv_syndrome, sv_data_error);
		}

		/* Flip a random parity bit */
		flipped = cpp_parity ^ (1 << (rand() % HAMMING_NUM_DATA));

		/* SystemVerilog */
		top->din = cpp_din;
		top->parity = flipped;
		top->eval();
		sv_dout = top->dout;
		sv_syndrome = top->hamming_dec__DOT__syndrome;
		sv_data_error = top->data_error;

		if (cpp_din != sv_dout) {
			printf("FAILED:\n");
			printf("Orig parity:              0x%01X\n", cpp_parity);
			printf("SV: Din:  0x%02X, parity: 0x%01X\n", cpp_din, flipped);
			printf("SV: Dout: 0x%02X (syndrome: 0x%02X, data error: %d)\n", sv_dout, sv_syndrome, sv_data_error);
		}
	}

	printf("Done!\n");

	delete top;
	return 0;
}
