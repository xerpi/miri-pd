module cache # (
	parameter SIZE = 4 * 1024 * 8,
	parameter LINE_SIZE = 32 * 8,
	parameter ADDR_SIZE = 32,
	parameter WORD_SIZE = 32,
	localparam NUM_LINES = SIZE / LINE_SIZE,
	localparam WORDS_PER_LINE = LINE_SIZE / WORD_SIZE,
	localparam INDEX_BITS = $clog2(NUM_LINES),
	localparam WORD_BITS = $clog2(WORDS_PER_LINE),
	localparam WOFF_BITS = $clog2(WORD_SIZE / 8),
	localparam TAG_BITS = ADDR_SIZE - INDEX_BITS - WORD_BITS - WOFF_BITS
) (
	input logic clk_i,
	input logic reset_i,
	cache_interface.slave cache_bus,
	memory_interface.master memory_bus
);
	typedef struct packed {
		logic [TAG_BITS - 1 : 0] tag;
		logic [INDEX_BITS - 1 : 0] index;
		logic [WORD_BITS - 1 : 0] word;
		logic [WOFF_BITS - 1 : 0] woff;
	} cache_addr_t;

	typedef struct packed {
		logic valid;
		logic dirty;
		logic [WORDS_PER_LINE - 1 : 0][WORD_SIZE - 1 : 0] data;
		logic [TAG_BITS - 1 : 0] tag;
	} cache_line_t;

	typedef enum logic [1:0] {
		IDLE,
		FILL,
		WRITEBACK
	} cache_state_t;

	/* Registers */
	cache_line_t lines[NUM_LINES - 1 : 0];
	cache_state_t state;

	/* Wires */
	cache_state_t next_state;
	cache_addr_t cpu_addr;
	cache_line_t cur_line;
	cache_line_t next_line;
	logic is_miss;
	logic needs_mem_access;
	logic cache_wr_enable;

	assign cpu_addr = cache_bus.addr;
	assign cur_line = lines[cpu_addr.index];

	assign is_miss = !cur_line.valid || (cpu_addr.tag != cur_line.tag);
	assign needs_mem_access = cache_bus.valid && is_miss;

	assign cache_bus.rd_data = cur_line.data[cpu_addr.word];
	assign cache_bus.miss = is_miss;

	/* FSM next state logic */
	always_comb begin
		next_state = state;

		priority case (state)
		IDLE: begin
			if (memory_bus.ready && needs_mem_access) begin
				if (cur_line.dirty)
					next_state = WRITEBACK;
				else
					next_state = FILL;
			end
		end
		FILL: begin
			if (memory_bus.ready)
				next_state = IDLE;
		end
		WRITEBACK: begin
			if (memory_bus.ready)
				next_state = FILL;
		end
		endcase
	end

	/* FSM output logic */
	always_comb begin
		priority case (state)
		IDLE: begin
			cache_bus.ready = memory_bus.ready;
			cache_wr_enable = cache_bus.valid && cache_bus.write && !is_miss;
			memory_bus.addr = 0;
			memory_bus.wr_data = 0;
			memory_bus.write = 0; // cur_line.valid && (cpu_addr.tag != cur_line.tag) for Writeback
			memory_bus.valid = memory_bus.ready && needs_mem_access;
		end
		FILL: begin
			cache_bus.ready = 0;
			cache_wr_enable = memory_bus.ready;
			memory_bus.addr = cache_bus.addr;
			memory_bus.wr_data = 0;
			memory_bus.write = 0;
			memory_bus.valid = 0;
		end
		WRITEBACK:  begin
			cache_bus.ready = 0;
			cache_wr_enable = memory_bus.ready;
			memory_bus.addr = {cur_line.tag, cpu_addr.index,
				{WORD_BITS + WOFF_BITS{1'b0}}};
			memory_bus.wr_data = cur_line.data;
			memory_bus.write = !memory_bus.ready;
			memory_bus.valid = memory_bus.ready;
		end
		endcase
	end

	always_comb begin
		priority case (state)
		IDLE: begin
			next_line.data[cpu_addr.word] = cache_bus.wr_data;
			next_line.tag = cur_line.tag;
			next_line.valid = cur_line.valid;
			next_line.dirty = cur_line.dirty || cache_wr_enable;
		end
		FILL: begin
			next_line.data = memory_bus.rd_data;
			if (cache_bus.write)
				next_line.data[cpu_addr.word] = cache_bus.wr_data;
			next_line.tag = cpu_addr.tag;
			next_line.valid = 1;
			next_line.dirty = 0; // cache_bus.write ??
		end
		WRITEBACK:  begin
			next_line.data = cur_line.data;
			next_line.tag = cur_line.tag;
			next_line.valid = cur_line.valid;
			next_line.dirty = 0;
		end
		endcase
	end

	/* FSM flip-flop */
	always_ff @ (posedge clk_i) begin
		if (reset_i)
			state <= IDLE;
		else
			state <= next_state;
	end

	always_ff @ (posedge clk_i) begin
		if (reset_i) begin
			for (integer i = 0; i < NUM_LINES; i++)
				lines[i] <= '0;
		end else if (cache_wr_enable) begin
			lines[cpu_addr.index] <= next_line;
		end
	end
endmodule
