add wave -radix 16 sim:/testbench/reset
add wave -radix 16 sim:/testbench/clk

add wave -radix 16 -expand -group {Cache bus} sim:/testbench/cache_bus.addr
add wave -radix 16 -expand -group {Cache bus} sim:/testbench/cache_bus.rd_data
add wave -radix 16 -expand -group {Cache bus} sim:/testbench/cache_bus.wr_data
add wave -radix 16 -expand -group {Cache bus} sim:/testbench/cache_bus.wr_size
add wave -radix 16 -expand -group {Cache bus} sim:/testbench/cache_bus.write
add wave -radix 16 -expand -group {Cache bus} sim:/testbench/cache_bus.access
add wave -radix 16 -expand -group {Cache bus} sim:/testbench/cache_bus.hit

add wave -radix 16 -expand -group {Cache} sim:/testbench/cache/state
add wave -radix 16 -expand -group {Cache} sim:/testbench/cache/hit
add wave -radix 16 -expand -group {Cache} sim:/testbench/cache/lines

add wave -radix 16 -expand -group {Memory bus} sim:/testbench/memory_bus.addr
add wave -radix 16 -expand -group {Memory bus} sim:/testbench/memory_bus.rd_data
add wave -radix 16 -expand -group {Memory bus} sim:/testbench/memory_bus.wr_data
add wave -radix 16 -expand -group {Memory bus} sim:/testbench/memory_bus.write
add wave -radix 16 -expand -group {Memory bus} sim:/testbench/memory_bus.valid
add wave -radix 16 -expand -group {Memory bus} sim:/testbench/memory_bus.ready
