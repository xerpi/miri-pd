import cache_interface_types::*;

module testbench();
	localparam MEMORY_SIZE = 64 * 1024 * 8;
	localparam MEMORY_DELAY_CYCLES = 5;
	localparam CACHE_SIZE = 4 * 1024 * 8;
	localparam LINE_SIZE = 32 * 8;
	localparam WORD_SIZE = 32;
	localparam ADDR_SIZE = 32;

	logic clk;
	logic reset;

	memory_interface # (
		.ADDR_SIZE(ADDR_SIZE),
		.LINE_SIZE(LINE_SIZE)
	) memory_bus();

	cache_interface # (
		.ADDR_SIZE(ADDR_SIZE),
		.WORD_SIZE(WORD_SIZE)
	) cache_bus();

	memory # (
		.SIZE(MEMORY_SIZE),
		.LINE_SIZE(LINE_SIZE),
		.ADDR_SIZE(ADDR_SIZE),
		.DELAY_CYCLES(MEMORY_DELAY_CYCLES)
	) memory (
		.clk_i(clk),
		.reset_i(reset),
		.memory_bus(memory_bus)
	);

	cache # (
		.SIZE(CACHE_SIZE),
		.LINE_SIZE(LINE_SIZE),
		.WORD_SIZE(WORD_SIZE),
		.ADDR_SIZE(ADDR_SIZE)
	) cache (
		.clk_i(clk),
		.reset_i(reset),
		.cache_bus(cache_bus),
		.memory_bus(memory_bus)
	);

	initial begin
		clk = 1'b1;
		reset = 1'b1;
		#50 clk = ~clk;
		reset = 1'b0;
		forever #50 clk = ~clk;
	end

	initial begin
		cache_bus.addr = 0;
		cache_bus.wr_data = 0;
		cache_bus.wr_size = CACHE_ACCESS_SIZE_WORD;
		cache_bus.write = 0;
		cache_bus.access = 0;
		@(negedge reset);
		@(posedge clk);

		/* Load miss */
		cache_bus.addr = 'h00000100;
		cache_bus.write = 0;
		cache_bus.access = 1;

		@(posedge clk && cache_bus.hit);
		if (cache_bus.write) @(posedge clk);

		/* Write hit */
		cache_bus.addr = 'h00000100;
		cache_bus.wr_data = 'hAABBCCDD;
		cache_bus.wr_size = CACHE_ACCESS_SIZE_WORD;
		cache_bus.write = 1;
		cache_bus.access = 1;

		@(posedge clk && cache_bus.hit);
		if (cache_bus.write) @(posedge clk);

		/* Write hit */
		cache_bus.addr = 'h00000100;
		cache_bus.wr_data = 'hDDAADDAA;
		cache_bus.wr_size = CACHE_ACCESS_SIZE_WORD;
		cache_bus.write = 1;
		cache_bus.access = 1;

		@(posedge clk && cache_bus.hit);
		if (cache_bus.write) @(posedge clk);

		/* Write miss */
		cache_bus.addr = 'h00001104;
		cache_bus.wr_data = 'h11223344;
		cache_bus.wr_size = CACHE_ACCESS_SIZE_WORD;
		cache_bus.write = 1;
		cache_bus.access = 1;

		@(posedge clk && cache_bus.hit);
		if (cache_bus.write) @(posedge clk);

		/* Write hit */
		cache_bus.addr = 'h00001104;
		cache_bus.wr_data = 'hAABBCCDD;
		cache_bus.wr_size = CACHE_ACCESS_SIZE_BYTE;
		cache_bus.write = 1;
		cache_bus.access = 1;

		@(posedge clk && cache_bus.hit);
		if (cache_bus.write) @(posedge clk);

		/* Write hit */
		cache_bus.addr = 'h00001106;
		cache_bus.wr_data = 'h8899EEFF;
		cache_bus.wr_size = CACHE_ACCESS_SIZE_HALF;
		cache_bus.write = 1;
		cache_bus.access = 1;

		@(posedge clk && cache_bus.hit);
		if (cache_bus.write) @(posedge clk);

		/* Write miss */
		cache_bus.addr = 'h00000106;
		cache_bus.wr_data = 'h66778899;
		cache_bus.wr_size = CACHE_ACCESS_SIZE_HALF;
		cache_bus.write = 1;
		cache_bus.access = 1;

		@(posedge clk && cache_bus.hit);
		if (cache_bus.write) @(posedge clk);
	end
endmodule
