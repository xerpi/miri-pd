EESchema Schematic File Version 4
LIBS:ecc-cache-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Bus Line
	5300 3050 5300 3100
Wire Bus Line
	4550 2600 4550 2650
Wire Bus Line
	5300 2650 4550 2650
Connection ~ 4550 2650
Wire Bus Line
	4550 2650 4550 3100
Wire Wire Line
	6050 4100 6050 4450
Text Label 6050 4250 0    50   ~ 0
hit
Wire Bus Line
	5050 4250 5350 4250
Wire Bus Line
	5350 4250 5350 4100
Wire Bus Line
	4750 4250 4550 4250
Wire Bus Line
	4550 4250 4550 4100
Wire Wire Line
	5150 4650 5150 4950
Wire Bus Line
	4900 4650 4900 4850
Text Label 4600 4950 0    50   ~ 0
rd_data
Text Label 5150 4950 0    50   ~ 0
ecc_error
Wire Wire Line
	4000 4400 4000 2350
Wire Wire Line
	4000 2350 4200 2350
Wire Bus Line
	4650 2100 4650 1900
Text Label 4650 2050 0    50   ~ 0
wr_data
Wire Bus Line
	4450 2100 3550 2100
Wire Bus Line
	3550 2100 3550 4400
Wire Bus Line
	4900 4850 3700 4850
Wire Bus Line
	3700 4850 3700 4700
Connection ~ 4900 4850
Wire Bus Line
	4900 4850 4900 4950
Wire Wire Line
	4050 4400 4000 4400
Wire Wire Line
	4050 4700 4000 4700
Wire Wire Line
	4500 4400 4500 4650
Wire Wire Line
	4500 4650 4450 4650
Wire Wire Line
	4650 4650 4650 4750
Wire Wire Line
	4650 4750 4450 4750
$Comp
L ecc-cache-rescue:MUX2-ecc-cache-lab3 U?
U 1 1 5BE87677
P 4550 2350
F 0 "U?" H 5050 2400 50  0001 C CNN
F 1 "MUX2" H 5050 2500 50  0001 C CNN
F 2 "" H 4550 2350 50  0001 C CNN
F 3 "" H 4550 2350 50  0001 C CNN
	1    4550 2350
	1    0    0    -1  
$EndComp
$Comp
L ecc-cache-rescue:ecc-calc-ecc-cache-lab3 U?
U 1 1 5BE876E8
P 5300 2850
F 0 "U?" H 5650 3000 50  0001 C CNN
F 1 "ecc-calc" H 5100 2850 50  0000 L CNN
F 2 "" H 5300 2850 50  0001 C CNN
F 3 "" H 5300 2850 50  0001 C CNN
	1    5300 2850
	1    0    0    -1  
$EndComp
$Comp
L ecc-cache-rescue:ecc-cache-data-ecc-cache-lab3 U?
U 1 1 5BE87770
P 4550 3600
F 0 "U?" H 4550 3900 50  0001 C CNN
F 1 "ecc-cache-data" H 4550 4000 50  0001 C CNN
F 2 "" H 4550 3600 50  0001 C CNN
F 3 "" H 4550 3600 50  0001 C CNN
	1    4550 3600
	1    0    0    -1  
$EndComp
$Comp
L ecc-cache-rescue:ecc-cache-ecc-ecc-cache-lab3 U?
U 1 1 5BE877D9
P 5300 3600
F 0 "U?" H 5300 3900 50  0001 C CNN
F 1 "ecc-cache-ecc" H 5300 4000 50  0001 C CNN
F 2 "" H 5300 3600 50  0001 C CNN
F 3 "" H 5300 3600 50  0001 C CNN
	1    5300 3600
	1    0    0    -1  
$EndComp
$Comp
L ecc-cache-rescue:ecc-cache-tag-ecc-cache-lab3 U?
U 1 1 5BE878C7
P 6050 3600
F 0 "U?" H 6050 3900 50  0001 C CNN
F 1 "ecc-cache-tag" H 6050 4000 50  0001 C CNN
F 2 "" H 6050 3600 50  0001 C CNN
F 3 "" H 6050 3600 50  0001 C CNN
	1    6050 3600
	1    0    0    -1  
$EndComp
$Comp
L ecc-cache-rescue:ecc-correct-reg-ecc-cache-lab3 U?
U 1 1 5BE87AD8
P 3700 4550
F 0 "U?" H 4150 4700 50  0001 C CNN
F 1 "ecc-correct-reg" H 4050 4900 50  0001 C CNN
F 2 "" H 3700 4550 50  0001 C CNN
F 3 "" H 3700 4550 50  0001 C CNN
	1    3700 4550
	1    0    0    -1  
$EndComp
Connection ~ 4000 4400
$Comp
L ecc-cache-rescue:NOT-ecc-cache-lab3 U?
U 1 1 5BE87B3A
P 4300 4400
F 0 "U?" H 4450 4600 50  0001 C CNN
F 1 "NOT" H 4600 4550 50  0001 C CNN
F 2 "" H 4300 4400 50  0001 C CNN
F 3 "" H 4300 4400 50  0001 C CNN
	1    4300 4400
	1    0    0    -1  
$EndComp
$Comp
L ecc-cache-rescue:OR-ecc-cache-lab3 U?
U 1 1 5BE87B87
P 4250 4700
F 0 "U?" H 4400 4900 50  0001 C CNN
F 1 "OR" H 4550 4850 50  0001 C CNN
F 2 "" H 4250 4700 50  0001 C CNN
F 3 "" H 4250 4700 50  0001 C CNN
	1    4250 4700
	-1   0    0    -1  
$EndComp
$Comp
L ecc-cache-rescue:ecc-correct-ecc-cache-lab3 U?
U 1 1 5BE87DA2
P 4900 4450
F 0 "U?" H 5250 4600 50  0001 C CNN
F 1 "ecc-correct" H 4650 4450 50  0000 L CNN
F 2 "" H 4900 4450 50  0001 C CNN
F 3 "" H 4900 4450 50  0001 C CNN
	1    4900 4450
	1    0    0    -1  
$EndComp
$EndSCHEMATC
