import cache_interface_types::*;

module ecc_cache # (
	parameter SIZE = 4 * 1024 * 8,
	parameter LINE_SIZE = 32 * 8,
	parameter ADDR_SIZE = 32,
	parameter WORD_SIZE = 32,
	localparam NUM_LINES = SIZE / LINE_SIZE,
	localparam BYTES_PER_LINE = LINE_SIZE / 8,
	localparam WORDS_PER_LINE = LINE_SIZE / WORD_SIZE,
	localparam INDEX_BITS = $clog2(NUM_LINES),
	localparam WORD_BITS = $clog2(WORDS_PER_LINE),
	localparam WOFF_BITS = $clog2(WORD_SIZE / 8),
	localparam OFFSET_BITS = $clog2(BYTES_PER_LINE),
	localparam TAG_BITS = ADDR_SIZE - INDEX_BITS - OFFSET_BITS
) (
	input logic clk_i,
	input logic reset_i,
	cache_interface.slave cache_bus,
	memory_interface.master memory_bus
);
	localparam HAMMING_NUM_PARITY = 4; /* Hamming(15, 11) for 8 bits */
	localparam HAMMING_NUM_DATA = 2 ** HAMMING_NUM_PARITY - HAMMING_NUM_PARITY - 1;

	typedef struct packed {
		logic [TAG_BITS - 1 : 0] tag;
		logic [INDEX_BITS - 1 : 0] index;
		logic [OFFSET_BITS - 1 : 0] offset;
	} ecc_cache_addr_t;

	typedef enum logic [1:0] {
		READY,
		MEMORY_FILL,
		CORRECTION
	} ecc_cache_state_t;

	typedef struct packed {
		logic [BYTES_PER_LINE][HAMMING_NUM_PARITY - 1 : 0] ecc_bytes;
		logic valid;
	} ecc_cache_line_t;

	typedef struct packed {
		logic [ADDR_SIZE - 1 : 0] addr;
		logic [WORD_SIZE - 1 : 0] data;
	} ecc_correction_reg_t;

	/* Registers */
	ecc_cache_state_t state;
	ecc_cache_line_t lines[NUM_LINES];
	ecc_correction_reg_t correction_reg;

	/* Wires */
	ecc_cache_state_t next_state;
	ecc_cache_line_t cur_line;
	ecc_cache_line_t next_line;
	ecc_correction_reg_t next_correction_reg;
	ecc_cache_addr_t cpu_addr;
	logic [WORD_BITS - 1 : 0] cpu_addr_word;
	logic [WOFF_BITS - 1 : 0] cpu_addr_woff;
	logic [WORD_BITS - 1 : 0] correction_reg_addr_word;
	logic ecc_line_wr_enable;
	logic correction_reg_wr_enable;
	logic [BYTES_PER_LINE - 1 : 0] ecc_line_wr_mask;
	logic [HAMMING_NUM_DATA - 1 : 0] hamming_enc_data[BYTES_PER_LINE];
	logic [HAMMING_NUM_PARITY - 1 : 0] hamming_enc_parity[BYTES_PER_LINE];
	logic [HAMMING_NUM_DATA - 1 : 0] hamming_dec_din[BYTES_PER_LINE];
	logic [HAMMING_NUM_PARITY - 1 : 0] hamming_dec_parity[BYTES_PER_LINE];
	logic [HAMMING_NUM_DATA - 1 : 0] hamming_dec_dout[BYTES_PER_LINE];
	logic [BYTES_PER_LINE - 1 : 0]hamming_dec_data_error;
	logic [WORD_SIZE - 1 : 0] hamming_dec_dout_word;
	logic cur_word_has_errors;
	logic load_hit_with_errors;
	logic store_hit;
	logic fill_from_memory;

	cache_interface # (
		.ADDR_SIZE(ADDR_SIZE),
		.WORD_SIZE(WORD_SIZE)
	) base_cache_bus();

	cache # (
		.SIZE(SIZE),
		.LINE_SIZE(LINE_SIZE),
		.WORD_SIZE(WORD_SIZE),
		.ADDR_SIZE(ADDR_SIZE)
	) base_cache (
		.clk_i(clk_i),
		.reset_i(reset_i),
		.cache_bus(base_cache_bus),
		.memory_bus(memory_bus)
	);

	generate
		genvar i;
		for (i = 0; i < BYTES_PER_LINE; i++) begin
			hamming_enc # (
				.NUM_PARITY(HAMMING_NUM_PARITY)
			) hamming_enc (
				.data(hamming_enc_data[i]),
				.parity(hamming_enc_parity[i])
			);

			hamming_dec # (
				.NUM_PARITY(HAMMING_NUM_PARITY)
			) hamming_dec (
				.din(hamming_dec_din[i]),
				.parity(hamming_dec_parity[i]),
				.dout(hamming_dec_dout[i]),
				.data_error(hamming_dec_data_error[i])
			);
		end
	endgenerate

	assign cpu_addr = cache_bus.addr;
	assign cur_line = lines[cpu_addr.index];
	assign cpu_addr_word = cache_bus.addr[WORD_BITS +: WORD_BITS];
	assign cpu_addr_woff = cache_bus.addr[0 +: WOFF_BITS];
	assign correction_reg_addr_word = correction_reg.addr[WORD_BITS +: WORD_BITS];

	/* Calculate if the current accessed word has errors */
	assign cur_word_has_errors = cur_line.valid && |hamming_dec_data_error[cpu_addr_word +: 4];

	/* Check if we should go to CORRECTION state and write to the correction reg */
	assign load_hit_with_errors = cache_bus.access && !cache_bus.write &&
				     base_cache_bus.hit && cur_word_has_errors;

	/* Detects when the CPU performs a cache store hit */
	assign store_hit = cache_bus.access && cache_bus.write && base_cache_bus.hit;

	/* Detects when a memory fill is started (when mem de-asserts ready) */
	assign fill_from_memory = memory_bus.valid && !memory_bus.write && !memory_bus.ready;

	/* Assign the corrected data coming out from the decoder */
	always_comb begin
		for (integer i = 0; i < 4; i++)
			hamming_dec_dout_word[8 * i +: 8] = hamming_dec_dout[cpu_addr.offset + i][7:0];
	end

	/* Assign the correction register values */
	assign next_correction_reg.addr = {cpu_addr.tag, cpu_addr.index, cpu_addr_word, {WOFF_BITS{1'b0}}};
	assign next_correction_reg.data = hamming_dec_dout_word;

	/* Write ECC code multiplexer, selects between:
	    - ECC of Data from the CPU (store hit)
	    - ECC of Data from memory (load/store miss)
	    - Data from the correction register (load hit with error)
	 */
	always_comb begin
		ecc_line_wr_mask = '0;
		for (integer i = 0; i < BYTES_PER_LINE; i++)
			hamming_enc_data[i] = '0;

		if (state == CORRECTION) begin /* Data from the correction register */
			ecc_line_wr_mask = 4'b1111 << correction_reg_addr_word;
			for (integer i = 0; i < 4; i++) begin
				hamming_enc_data[4 * correction_reg_addr_word + i] =
					{'0, correction_reg.data[8 * i +: 8]};
			end
		end else if (cache_bus.access && cache_bus.write && base_cache_bus.hit) begin /* ECC of Data from the CPU (store hit) */
			priority case (cache_bus.wr_size)
			CACHE_ACCESS_SIZE_BYTE: begin
				hamming_enc_data[cpu_addr.offset] = {'0, cache_bus.wr_data[8 * cpu_addr_woff +: 8]};
				ecc_line_wr_mask[cpu_addr.offset] = 1;
			end
			CACHE_ACCESS_SIZE_HALF: begin
				hamming_enc_data[cpu_addr.offset] = {'0, cache_bus.wr_data[8 * cpu_addr_woff +: 8]};
				hamming_enc_data[cpu_addr.offset + 1] = {'0, cache_bus.wr_data[8 * (cpu_addr_woff + 1) +: 8]};
				ecc_line_wr_mask[cpu_addr.offset +: 2] = {2{1'b1}};
			end
			CACHE_ACCESS_SIZE_WORD: begin
				hamming_enc_data[cpu_addr.offset] = {'0, cache_bus.wr_data[8 * cpu_addr_woff +: 8]};
				hamming_enc_data[cpu_addr.offset + 1] = {'0, cache_bus.wr_data[8 * (cpu_addr_woff + 1) +: 8]};
				hamming_enc_data[cpu_addr.offset + 2] = {'0, cache_bus.wr_data[8 * (cpu_addr_woff + 2) +: 8]};
				hamming_enc_data[cpu_addr.offset + 3] = {'0, cache_bus.wr_data[8 * (cpu_addr_woff + 3) +: 8]};
				ecc_line_wr_mask[cpu_addr.offset +: 4] = {4{1'b1}};
			end
			endcase
		end else begin /* Data from memory: full ECC line */
			ecc_line_wr_mask = '1;
			for (integer i = 0; i < BYTES_PER_LINE; i++) begin
				hamming_enc_data[i] = {'0, memory_bus.rd_data[8 * i +: 8]};
			end
		end
	end

	/* Next ECC line */
	always_comb begin
		next_line.ecc_bytes = cur_line.ecc_bytes;
		/* Once a line is written, it's always valid, since even
		 * when the cache performs a writeback, the line is still valid
		 * and it will fill that line afterwards: then we will update the ECC.
		 */
		next_line.valid = 1;
		for (integer i = 0; i < BYTES_PER_LINE; i++) begin
			if (ecc_line_wr_mask[i])
				next_line.ecc_bytes[i] = hamming_enc_parity[i];
		end
	end

	/* Route data to be decoded */
	always_comb begin
		for (integer i = 0; i < BYTES_PER_LINE; i++)
			hamming_dec_din[i] = '0;

		for (integer i = 0; i < 4; i++)
			hamming_dec_din[4 * cpu_addr_word + i] = {'0, base_cache_bus.rd_data[8 * i +: 8]};

		for (integer i = 0; i < BYTES_PER_LINE; i++)
			hamming_dec_parity[i] = cur_line.ecc_bytes[i];
	end

	/* FSM next state logic */
	always_comb begin
		next_state = state;
		priority case (state)
		READY: begin
			if (load_hit_with_errors)
				next_state = CORRECTION;
			else if (fill_from_memory)
				next_state = MEMORY_FILL;
		end
		MEMORY_FILL: begin
			if (memory_bus.ready)
				next_state = READY;
		end
		CORRECTION: begin
			next_state = READY;
		end
		endcase
	end

	/* FSM output logic */
	always_comb begin
		ecc_line_wr_enable = 0;
		correction_reg_wr_enable = 0;

		priority case (state)
		READY: begin
			base_cache_bus.addr = cache_bus.addr;
			cache_bus.rd_data = base_cache_bus.rd_data;
			base_cache_bus.wr_data = cache_bus.wr_data;
			base_cache_bus.wr_size = cache_bus.wr_size;
			base_cache_bus.write = cache_bus.write;
			base_cache_bus.access = cache_bus.access;
			cache_bus.hit = base_cache_bus.hit;

			/* If load hit with errors -> CORRECTION */
			if (load_hit_with_errors) begin
				correction_reg_wr_enable = 1;
				/* Send the corrected data to the CPU */
				cache_bus.rd_data = hamming_dec_dout_word;
			end else if (store_hit) begin /* Update the ECC line when store hit */
				ecc_line_wr_enable = 1;
			end
		end
		MEMORY_FILL: begin
			base_cache_bus.addr = cache_bus.addr;
			cache_bus.rd_data = base_cache_bus.rd_data;
			base_cache_bus.wr_data = cache_bus.wr_data;
			base_cache_bus.wr_size = cache_bus.wr_size;
			base_cache_bus.write = cache_bus.write;
			base_cache_bus.access = cache_bus.access;
			cache_bus.hit = base_cache_bus.hit;

			/* When the fill from memory is done, update the ECC line */
			if (memory_bus.ready)
				ecc_line_wr_enable = 1;
		end
		CORRECTION: begin
			/* We don't care about this one... */
			cache_bus.rd_data = base_cache_bus.rd_data;
			/* Perform a write from the correction register */
			base_cache_bus.addr = correction_reg.addr;
			base_cache_bus.wr_data = correction_reg.data;
			base_cache_bus.wr_size = CACHE_ACCESS_SIZE_WORD;
			base_cache_bus.write = 1;
			base_cache_bus.access = 1;
			/* Fake a miss to the CPU */
			cache_bus.hit = 0;
			/* Calculate and write the new ECC line for the corrected data */
			ecc_line_wr_enable = 1;
		end
		endcase
	end

	/* FSM flip-flop */
	always_ff @ (posedge clk_i) begin
		if (reset_i)
			state <= READY;
		else
			state <= next_state;
	end

	/* Correction register */
	always_ff @ (posedge clk_i) begin
		if (reset_i)
			correction_reg <= '0;
		else if (correction_reg_wr_enable)
			correction_reg <= next_correction_reg;
	end

	/* ECC lines registers */
	always_ff @ (posedge clk_i) begin
		if (reset_i) begin
			for (integer i = 0; i < NUM_LINES; i++)
				lines[i] <= '0;
		end else if (ecc_line_wr_enable) begin
				lines[cpu_addr.index] <= next_line;
		end
	end
endmodule
