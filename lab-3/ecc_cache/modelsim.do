add wave -radix 16 sim:/testbench/reset
add wave -radix 16 sim:/testbench/clk

add wave -radix 16 -expand -group {Cache bus} sim:/testbench/cache_bus.addr
add wave -radix 16 -expand -group {Cache bus} sim:/testbench/cache_bus.rd_data
add wave -radix 16 -expand -group {Cache bus} sim:/testbench/cache_bus.wr_data
add wave -radix 16 -expand -group {Cache bus} sim:/testbench/cache_bus.wr_size
add wave -radix 16 -expand -group {Cache bus} sim:/testbench/cache_bus.write
add wave -radix 16 -expand -group {Cache bus} sim:/testbench/cache_bus.access
add wave -radix 16 -expand -group {Cache bus} sim:/testbench/cache_bus.hit

add wave -radix 16 -expand -group {ECC cache} sim:/testbench/ecc_cache/state
add wave -radix 16 -expand -group {ECC cache} sim:/testbench/ecc_cache/hamming_dec_data_error
add wave -radix 16 -expand -group {ECC cache} sim:/testbench/ecc_cache/cur_word_has_errors
add wave -radix 16 -expand -group {ECC cache} sim:/testbench/ecc_cache/load_hit_with_errors
add wave -radix 16 -expand -group {ECC cache} sim:/testbench/ecc_cache/correction_reg
add wave -radix 16 -expand -group {ECC cache} sim:/testbench/ecc_cache/ecc_line_wr_enable
add wave -radix 16 -expand -group {ECC cache} sim:/testbench/ecc_cache/ecc_line_wr_mask
add wave -radix 16 -expand -group {ECC cache} sim:/testbench/ecc_cache/hamming_enc_data
add wave -radix 16 -expand -group {ECC cache} sim:/testbench/ecc_cache/hamming_enc_parity
add wave -radix 16 -expand -group {ECC cache} sim:/testbench/ecc_cache/lines

add wave -radix 16 -expand -group {Base cache} sim:/testbench/ecc_cache/base_cache/state
add wave -radix 16 -expand -group {Base cache} sim:/testbench/ecc_cache/base_cache/hit
add wave -radix 16 -expand -group {Base cache} sim:/testbench/ecc_cache/base_cache/lines

add wave -radix 16 -expand -group {Memory bus} sim:/testbench/memory_bus.addr
add wave -radix 16 -expand -group {Memory bus} sim:/testbench/memory_bus.rd_data
add wave -radix 16 -expand -group {Memory bus} sim:/testbench/memory_bus.wr_data
add wave -radix 16 -expand -group {Memory bus} sim:/testbench/memory_bus.write
add wave -radix 16 -expand -group {Memory bus} sim:/testbench/memory_bus.valid
add wave -radix 16 -expand -group {Memory bus} sim:/testbench/memory_bus.ready
