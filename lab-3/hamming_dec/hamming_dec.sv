module hamming_dec #(
	parameter NUM_PARITY = 3,
	localparam NUM_DATA = 2 ** NUM_PARITY - NUM_PARITY - 1,
	localparam NUM_TOTAL = 2 ** NUM_PARITY - 1
) (
	input logic [NUM_DATA - 1 : 0] din,
	input logic [NUM_PARITY - 1 : 0] parity,
	output logic [NUM_DATA - 1 : 0] dout,
	output logic data_error
);
	logic [NUM_PARITY - 1 : 0][NUM_DATA - 1 : 0] matrix;
	logic [NUM_PARITY - 1 : 0] syndrome;
	logic [NUM_TOTAL : 0] decoder;

	function automatic is_data_bit(input integer i);
		return i != (i & ~(i - 1));
	endfunction

	function automatic integer data_bit_index(input integer d);
		integer i = 0, j = 0;
		while (i < NUM_TOTAL && j < d) begin
			i++;
			if (is_data_bit(i))
				j++;
		end
		return i;
	endfunction

	function automatic parity_covers(input integer p, input integer d);
		return ((data_bit_index(d) >> (p - 1)) & 1) == 1;
	endfunction

	genvar i, j;

	generate
		for (i = 0; i < NUM_PARITY; i++) begin
			for (j = 0; j < NUM_DATA; j++) begin
				if (parity_covers(i + 1, j + 1))
					assign matrix[i][j] = din[j];
				else
					assign matrix[i][j] = 0;
			end
			assign syndrome[i] = ^matrix[i] ^ parity[i];
		end
	endgenerate

	assign decoder = 1 << syndrome;
	assign data_error = syndrome && is_data_bit(syndrome);

	generate
		for (i = 0; i < NUM_DATA; i++) begin
			assign dout[i] = din[i] ^ decoder[data_bit_index(i + 1)];
		end
	endgenerate
endmodule
