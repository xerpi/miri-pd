module ecc_regfile(
	input logic clk_i,
	input logic reset_i,
	/* Original regfile signals */
	input logic [4:0] rd_addr1_i,
	input logic [4:0] rd_addr2_i,
	input logic [4:0] wr_addr_i,
	input logic [31:0] wr_data_i,
	input logic wr_en_i,
	output logic [31:0] rd_data1_o,
	output logic [31:0] rd_data2_o,
	/* New signals for ECC regfile */
	input logic rd_en1_i,
	input logic rd_en2_i,
	output logic ready_o,
	output logic reg1_double_error_o,
	output logic reg2_double_error_o
);
	localparam NUM_REGISTERS = 31;
	localparam NUM_RD_PORTS	= 2;
	localparam REGISTER_SIZE = 32;
	localparam REGISTER_BITS = $clog2(NUM_REGISTERS);
	localparam HAMMING_NUM_PARITY = 6; /* Hamming(63, 57) for 32 bits */
	localparam HAMMING_NUM_DATA = 2 ** HAMMING_NUM_PARITY - HAMMING_NUM_PARITY - 1;

	typedef enum logic [1:0] {
		READY,
		CORRECTION_REG_1,
		CORRECTION_REG_2,
		CORRECTION_REG_1_2
	} ecc_regfile_state_t;

	typedef struct packed {
		logic [HAMMING_NUM_PARITY - 1 : 0] ecc;
		logic extra_parity;
		logic valid;
	} ecc_regfile_reg_ecc_t;

	typedef struct packed {
		logic [REGISTER_BITS - 1 : 0] addr;
		logic [REGISTER_SIZE - 1 : 0] data;
	} ecc_correction_reg_t;

	/* Hamming encoder signals */
	logic [HAMMING_NUM_DATA   - 1 : 0] hamming_enc_data;
	logic [HAMMING_NUM_PARITY - 1 : 0] hamming_enc_parity;
	logic                              hamming_enc_extra_parity;
	/* Hamming decoder signals */
	logic [HAMMING_NUM_DATA   - 1 : 0] hamming_dec_din[NUM_RD_PORTS];
	logic [HAMMING_NUM_PARITY - 1 : 0] hamming_dec_parity[NUM_RD_PORTS];
	logic                              hamming_dec_extra_parity[NUM_RD_PORTS];
	logic [HAMMING_NUM_DATA   - 1 : 0] hamming_dec_dout[NUM_RD_PORTS];
	logic [NUM_RD_PORTS      - 1 : 0] hamming_dec_data_error;
	logic [NUM_RD_PORTS      - 1 : 0] hamming_dec_double_error;

	/* Base regfile signals */
	logic [4:0] base_rd_addr1;
	logic [4:0] base_rd_addr2;
	logic [4:0] base_wr_addr;
	logic [31:0] base_wr_data;
	logic base_wr_en;
	logic [31:0] base_rd_data1;
	logic [31:0] base_rd_data2;

	/* ECC regfile registers */
	ecc_regfile_state_t state = READY;
	ecc_regfile_reg_ecc_t reg_eccs[1 : NUM_REGISTERS];
	ecc_correction_reg_t correction_reg_1;
	ecc_correction_reg_t correction_reg_2;

	/* Private wires */
	ecc_regfile_state_t next_state;
	ecc_regfile_reg_ecc_t next_reg_ecc;
	ecc_regfile_reg_ecc_t cur_reg_ecc_1;
	ecc_regfile_reg_ecc_t cur_reg_ecc_2;
	ecc_correction_reg_t next_correction_reg_1;
	ecc_correction_reg_t next_correction_reg_2;
	logic reg_ecc_wr_enable;
	logic [REGISTER_BITS - 1 : 0] reg_ecc_wr_addr;
	logic correction_reg_1_wr_enable;
	logic correction_reg_2_wr_enable;
	logic reg1_read_with_errors;
	logic reg2_read_with_errors;

	regfile base_regfile (
		.clk_i(clk_i),
		.reset_i(reset_i),
		.rd_addr1_i(base_rd_addr1),
		.rd_addr2_i(base_rd_addr2),
		.wr_addr_i(base_wr_addr),
		.wr_data_i(base_wr_data),
		.wr_en_i(base_wr_en),
		.rd_data1_o(base_rd_data1),
		.rd_data2_o(base_rd_data2)
	);

	hamming_enc # (
		.NUM_PARITY(HAMMING_NUM_PARITY)
	) hamming_enc (
		.data(hamming_enc_data),
		.parity(hamming_enc_parity),
		.extra_parity(hamming_enc_extra_parity)
	);

	generate
		genvar i;
		for (i = 0; i < NUM_RD_PORTS; i++) begin
			hamming_dec # (
				.NUM_PARITY(HAMMING_NUM_PARITY)
			) hamming_dec (
				.din(hamming_dec_din[i]),
				.parity(hamming_dec_parity[i]),
				.extra_parity(hamming_dec_extra_parity[i]),
				.dout(hamming_dec_dout[i]),
				.data_error(hamming_dec_data_error[i]),
				.double_error(hamming_dec_double_error[i])
			);
		end
	endgenerate

	/* Private wires calculation */
	assign cur_reg_ecc_1 = reg_eccs[rd_addr1_i];
	assign cur_reg_ecc_2 = reg_eccs[rd_addr2_i];
	assign reg1_read_with_errors = rd_en1_i && (rd_addr1_i != 0) &&
		cur_reg_ecc_1.valid && hamming_dec_data_error[0];
	assign reg2_read_with_errors = rd_en2_i && (rd_addr2_i != 0) &&
		cur_reg_ecc_2.valid && hamming_dec_data_error[1];

	/* Double error detection */
	assign reg1_double_error_o = rd_en1_i && cur_reg_ecc_1.valid && hamming_dec_double_error[0];
	assign reg2_double_error_o = rd_en2_i && cur_reg_ecc_2.valid && hamming_dec_double_error[1];

	/* Route data to be decoded */
	assign hamming_dec_din[0] = {'0, base_rd_data1};
	assign hamming_dec_din[1] = {'0, base_rd_data2};
	assign hamming_dec_parity[0] = cur_reg_ecc_1.ecc;
	assign hamming_dec_parity[1] = cur_reg_ecc_2.ecc;
	assign hamming_dec_extra_parity[0] = cur_reg_ecc_1.extra_parity;
	assign hamming_dec_extra_parity[1] = cur_reg_ecc_2.extra_parity;

	/* Next ECC line */
	assign next_reg_ecc.valid = 1;
	assign next_reg_ecc.ecc = hamming_enc_parity;
	assign next_reg_ecc.extra_parity = hamming_enc_extra_parity;

	/* Assign the correction registers values */
	assign next_correction_reg_1.addr = rd_addr1_i;
	assign next_correction_reg_2.addr = rd_addr2_i;
	assign next_correction_reg_1.data = hamming_dec_dout[0];
	assign next_correction_reg_2.data = hamming_dec_dout[1];

	/* FSM next state logic */
	always_comb begin
		next_state = state;
		priority case (state)
		READY: begin
			if (reg1_read_with_errors && reg2_read_with_errors)
				next_state = CORRECTION_REG_1_2;
			else if (reg1_read_with_errors)
				next_state = CORRECTION_REG_1;
			else if (reg2_read_with_errors)
				next_state = CORRECTION_REG_2;
		end
		CORRECTION_REG_1,
		CORRECTION_REG_2:
			next_state = READY;
		CORRECTION_REG_1_2:
			next_state = CORRECTION_REG_2;
		endcase
	end

	/* FSM output logic */
	always_comb begin
		reg_ecc_wr_enable = 0;
		correction_reg_1_wr_enable = 0;
		correction_reg_2_wr_enable = 0;
		priority case (state)
		READY: begin
			base_rd_addr1 = rd_addr1_i;
			base_rd_addr2 = rd_addr2_i;
			base_wr_addr = wr_addr_i;
			base_wr_data = wr_data_i;
			base_wr_en = wr_en_i;
			rd_data1_o = base_rd_data1;
			rd_data2_o = base_rd_data2;
			ready_o = 1; /* If there is an error we won't be ready next cycle */

			/* If there's a regfile write, update ECC */
			reg_ecc_wr_enable = wr_en_i && (wr_addr_i != 0);
			reg_ecc_wr_addr = wr_addr_i;
			/* Route Hamming encoder */
			hamming_enc_data = wr_data_i;

			/* If read with errors -> CORRECTION */
			if (reg1_read_with_errors) begin
				correction_reg_1_wr_enable = 1;
				/* Send the corrected data to the CPU */
				rd_data1_o = hamming_dec_dout[0];
			end
			if (reg2_read_with_errors) begin
				correction_reg_2_wr_enable = 1;
				/* Send the corrected data to the CPU */
				rd_data2_o = hamming_dec_dout[1];
			end
		end
		CORRECTION_REG_1,
		CORRECTION_REG_1_2: begin
			ready_o = 0;
			/* We are updating the ECC */
			reg_ecc_wr_enable = 1;
			reg_ecc_wr_addr = correction_reg_1.addr;
			hamming_enc_data = correction_reg_1.data;
			/* Get correction reg 1 info for base regfile */
			base_wr_addr = correction_reg_1.addr;
			base_wr_data = correction_reg_1.data;
			base_wr_en = 1;
		end
		CORRECTION_REG_2: begin
			ready_o = 0;
			/* We are updating the ECC */
			reg_ecc_wr_enable = 1;
			reg_ecc_wr_addr = correction_reg_2.addr;
			hamming_enc_data = correction_reg_2.data;
			/* Get correction reg 2 info for base regfile */
			base_wr_addr = correction_reg_2.addr;
			base_wr_data = correction_reg_2.data;
			base_wr_en = 1;
		end
		endcase
	end

	/* FSM flip-flop */
	always_ff @ (posedge clk_i) begin
		if (reset_i)
			state <= READY;
		else
			state <= next_state;
	end

	/* Correction registers */
	always_ff @ (posedge clk_i) begin
		if (reset_i) begin
			correction_reg_1 <= '0;
			correction_reg_2 <= '0;
		end else begin
			if (correction_reg_1_wr_enable)
				correction_reg_1 <= next_correction_reg_1;
			if (correction_reg_2_wr_enable)
				correction_reg_2 <= next_correction_reg_2;
		end
	end

	/* ECC data registers */
	always_ff @ (posedge clk_i) begin
		if (reset_i) begin
			for (integer i = 1; i <= NUM_REGISTERS; i++)
				reg_eccs[i] <= '0;
		end else begin
			if (reg_ecc_wr_enable)
				reg_eccs[reg_ecc_wr_addr] <= next_reg_ecc;
		end
	end

endmodule
