module testbench();
	logic clk;
	logic reset;

	/* Original regfile signals */
	logic [4:0] rd_addr1;
	logic [4:0] rd_addr2;
	logic [4:0] wr_addr;
	logic [31:0] wr_data;
	logic wr_en;
	logic [31:0] rd_data1;
	logic [31:0] rd_data2;
	/* New signals for ECC regfile */
	logic rd_en1;
	logic rd_en2;
	logic ready;
	logic reg1_double_error;
	logic reg2_double_error;

	ecc_regfile ecc_regfile (
		.clk_i(clk),
		.reset_i(reset),
		/* Original regfile signals */
		.rd_addr1_i(rd_addr1),
		.rd_addr2_i(rd_addr2),
		.wr_addr_i(wr_addr),
		.wr_data_i(wr_data),
		.wr_en_i(wr_en),
		.rd_data1_o(rd_data1),
		.rd_data2_o(rd_data2),
		/* New signals for ECC regfile */
		.rd_en1_i(rd_en1),
		.rd_en2_i(rd_en2),
		.ready_o(ready),
		.reg1_double_error_o(reg1_double_error),
		.reg2_double_error_o(reg2_double_error)
	);

	initial begin
		clk = 1'b1;
		reset = 1'b1;
		#50 clk = ~clk;
		reset = 1'b0;
		forever #50 clk = ~clk;
	end

	initial begin
		rd_addr1 = 'h0;
		rd_addr2 = 'h0;
		wr_addr = 'h0;
		wr_data = 'h0;
		wr_en = 0;
		rd_en1 = 0;
		rd_en2 = 0;
		@(negedge reset);
		@(posedge clk);

		/* Write to register 1 */
		wr_addr = 1;
		wr_data = 'h0D0D0D0D; /* 0b1101 */
		wr_en = 1;
		@(posedge clk);

		/* Write to register 2 */
		wr_addr = 2;
		wr_data = 'hEEEEEEEE; /* 0b1110 */
		wr_en = 1;
		@(posedge clk);

		/* Stop writing */
		wr_addr = 'h0;
		wr_data = 'h0;
		wr_en = 0;
		@(posedge clk);

		/* Simulate data bit flip on register 1 */
		ecc_regfile.base_regfile.registers[1] = 'h0D0D0D0C; /* 0x0D to 0x0C */
		@(posedge clk);

		/* Read register 1 with port 1 (with ECC correct) */
		rd_addr1 = 'h1;
		rd_en1 = 1;
		@(posedge clk);
		rd_addr1 = 'h0;
		rd_en1 = 0;
		@(posedge clk);
		@(posedge clk);

		/* Simulate data bit flip on register 2 */
		ecc_regfile.base_regfile.registers[2] = 'hEEFEEEEE; /* 0xEE to 0xFE */
		@(posedge clk);

		/* Read register 2 with port 2 (with ECC correct) */
		rd_addr2 = 'h2;
		rd_en2 = 1;
		@(posedge clk);
		rd_addr2 = 'h0;
		rd_en2 = 0;
		@(posedge clk);
		@(posedge clk);

		/* Simulate data bit flip on register 1 and 2 */
		ecc_regfile.base_regfile.registers[1] = 'h0D0D0C0D; /* 0x0D to 0x0C */
		ecc_regfile.base_regfile.registers[2] = 'hEEEEEEFE; /* 0xEE to 0xFE */
		@(posedge clk);

		/* Read reg 1 with port 2, and reg 2 with port 1 (with ECC correct) */
		rd_addr1 = 'h2;
		rd_addr2 = 'h1;
		rd_en1 = 1;
		rd_en2 = 1;
		@(posedge clk);
		rd_addr1 = 'h0;
		rd_addr2 = 'h0;
		rd_en1 = 0;
		rd_en2 = 0;
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);

		/* Simulate *DOUBLE* data bit flip on register 1 */
		ecc_regfile.base_regfile.registers[1] = 'h0D0C0D0C; /* 0x0D to 0x0C */
		@(posedge clk);

		/* Read register 1 with port 1 (double error) */
		rd_addr1 = 'h1;
		rd_en1 = 1;
		@(posedge clk);
		rd_addr1 = 'h0;
		rd_en1 = 0;
		@(posedge clk);
		@(posedge clk);
	end
endmodule
