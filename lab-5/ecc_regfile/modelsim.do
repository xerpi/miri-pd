add wave -radix 16 sim:/testbench/reset
add wave -radix 16 sim:/testbench/clk

add wave -radix 16 -expand -group {ECC regfile} sim:/testbench/ecc_regfile/reset_i
add wave -radix 16 -expand -group {ECC regfile} sim:/testbench/ecc_regfile/rd_addr1_i
add wave -radix 16 -expand -group {ECC regfile} sim:/testbench/ecc_regfile/rd_addr2_i
add wave -radix 16 -expand -group {ECC regfile} sim:/testbench/ecc_regfile/wr_addr_i
add wave -radix 16 -expand -group {ECC regfile} sim:/testbench/ecc_regfile/wr_data_i
add wave -radix 16 -expand -group {ECC regfile} sim:/testbench/ecc_regfile/wr_en_i
add wave -radix 16 -expand -group {ECC regfile} sim:/testbench/ecc_regfile/rd_data1_o
add wave -radix 16 -expand -group {ECC regfile} sim:/testbench/ecc_regfile/rd_data2_o
add wave -radix 16 -expand -group {ECC regfile} sim:/testbench/ecc_regfile/rd_en1_i
add wave -radix 16 -expand -group {ECC regfile} sim:/testbench/ecc_regfile/rd_en2_i
add wave -radix 16 -expand -group {ECC regfile} sim:/testbench/ecc_regfile/ready_o
add wave -radix 16 -expand -group {ECC regfile} sim:/testbench/ecc_regfile/reg1_double_error_o
add wave -radix 16 -expand -group {ECC regfile} sim:/testbench/ecc_regfile/reg2_double_error_o
add wave -radix 16 -expand -group {ECC regfile} sim:/testbench/ecc_regfile/state
add wave -radix 16 -expand -group {ECC regfile} sim:/testbench/ecc_regfile/reg_eccs
add wave -radix 16 -expand -group {ECC regfile} sim:/testbench/ecc_regfile/correction_reg_1
add wave -radix 16 -expand -group {ECC regfile} sim:/testbench/ecc_regfile/correction_reg_2
add wave -radix 16 -expand -group {ECC regfile} sim:/testbench/ecc_regfile/reg1_read_with_errors
add wave -radix 16 -expand -group {ECC regfile} sim:/testbench/ecc_regfile/reg2_read_with_errors

add wave -radix 16 -expand -group {Base regfile} sim:/testbench/ecc_regfile/base_regfile/reset_i
add wave -radix 16 -expand -group {Base regfile} sim:/testbench/ecc_regfile/base_regfile/rd_addr1_i
add wave -radix 16 -expand -group {Base regfile} sim:/testbench/ecc_regfile/base_regfile/rd_addr2_i
add wave -radix 16 -expand -group {Base regfile} sim:/testbench/ecc_regfile/base_regfile/wr_addr_i
add wave -radix 16 -expand -group {Base regfile} sim:/testbench/ecc_regfile/base_regfile/wr_data_i
add wave -radix 16 -expand -group {Base regfile} sim:/testbench/ecc_regfile/base_regfile/wr_en_i
add wave -radix 16 -expand -group {Base regfile} sim:/testbench/ecc_regfile/base_regfile/rd_data1_o
add wave -radix 16 -expand -group {Base regfile} sim:/testbench/ecc_regfile/base_regfile/rd_data2_o
add wave -radix 16 -expand -group {Base regfile} sim:/testbench/ecc_regfile/base_regfile/registers
