#include <iostream>
#include <cstdio>
#include <cstdint>
#include <verilated.h>
#include "Vhamming_enc.h"

#define ARRAY_SIZE(x) (sizeof(x) / (sizeof *(x)))

/*
 * Hamming:
 * Given m parity bits:
 *   * 2^m - m - 1 data bits
 *   * 2^m - 1 total bits
 */
#define HAMMING_NUM_DATA ((1 << HAMMING_NUM_PARITY) - HAMMING_NUM_PARITY - 1)
#define HAMMING_NUM_TOTAL ((1 << HAMMING_NUM_PARITY) - 1)

static unsigned int binarray_to_uint(unsigned int data[], int size)
{
	unsigned int n = 0;

	for (int i = 0; i < size; i++)
		n |= data[i] << i;

	return n;
}

static void uint_to_binarray(unsigned int n, unsigned int data[], unsigned int size)
{
	for (unsigned int i = 0; i < size; i++)
		data[i] = (n >> i) & 1;
}

static void hamming_enc(const unsigned int data[HAMMING_NUM_DATA], unsigned int parity[HAMMING_NUM_PARITY], unsigned int *extra_parity)
{
	for (unsigned int p = 0; p < HAMMING_NUM_PARITY; p++) {
		unsigned int data_bit = 0;
		parity[p] = 0;
		for (unsigned int i = 1; i <= HAMMING_NUM_TOTAL; i++) {
			if (i != (i & ~(i - 1))) { /* is data bit */
				data_bit++;
				if (i & (1 << p))
					parity[p] ^= data[data_bit - 1];
			}
		}
	}

	/* Calculate extra parity bit */
	*extra_parity = 0;

	for (unsigned int i = 0; i < HAMMING_NUM_DATA; i++)
		*extra_parity ^= data[i];

	for (unsigned int i = 0; i < HAMMING_NUM_PARITY; i++)
		*extra_parity ^= parity[i];
}

int main(int argc, char *argv[])
{
	Verilated::commandArgs(argc, argv);

	Vhamming_enc *top = new Vhamming_enc;

	unsigned int data[HAMMING_NUM_DATA];
	unsigned int parity[HAMMING_NUM_PARITY];
	unsigned int cpp_extra_parity;

	for (unsigned long long i = 0; i < 1ULL << HAMMING_NUM_DATA; i++) {
		/* C++ */
		uint_to_binarray(i, data, ARRAY_SIZE(data));
		hamming_enc(data, parity, &cpp_extra_parity);
		unsigned int cpp_data = binarray_to_uint(data, ARRAY_SIZE(data));
		unsigned int cpp_parity = binarray_to_uint(parity, ARRAY_SIZE(parity));

		/* SystemVerilog */
		top->data = i;
		top->eval();
		unsigned int sv_data = top->data;
		unsigned int sv_parity = top->parity;
		unsigned int sv_extra_parity = top->extra_parity;

		if ((sv_parity != cpp_parity) || (sv_extra_parity != cpp_extra_parity)) {
			printf("FAILED:\n");
			printf("C++: Data: 0x%02X, parity: 0x%01X, extra parity: %d\n", cpp_data, cpp_parity, cpp_extra_parity);
			printf("SV:  Data: 0x%02X, parity: 0x%01X, extra parity: %d\n", sv_data, sv_parity, sv_extra_parity);
		}
	}

	printf("Test done!\n");

	delete top;
	return 0;
}
