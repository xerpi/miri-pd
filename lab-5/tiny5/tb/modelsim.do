force -freeze sim:/top/clk_i 1 0, 0 {50 ps} -r 100
force -freeze sim:/top/reset_i 1 0, 0 {100 ps} -freeze

add wave -radix 16 sim:/top/reset_i
add wave -radix 16 sim:/top/clk_i

add wave -radix 16 -expand -group {Datapath} sim:/top/datapath/pc
add wave -radix 16 -expand -group {Datapath} sim:/top/datapath/id_reg
add wave -radix 16 -expand -group {Datapath} sim:/top/datapath/ex_reg
add wave -radix 16 -expand -group {Datapath} sim:/top/datapath/mem_reg
add wave -radix 16 -expand -group {Datapath} sim:/top/datapath/wb_reg

add wave -radix 16 -expand -group {ECC regfile} sim:/top/datapath/regfile/reset_i
add wave -radix 16 -expand -group {ECC regfile} sim:/top/datapath/regfile/rd_addr1_i
add wave -radix 16 -expand -group {ECC regfile} sim:/top/datapath/regfile/rd_addr2_i
add wave -radix 16 -expand -group {ECC regfile} sim:/top/datapath/regfile/wr_addr_i
add wave -radix 16 -expand -group {ECC regfile} sim:/top/datapath/regfile/wr_data_i
add wave -radix 16 -expand -group {ECC regfile} sim:/top/datapath/regfile/wr_en_i
add wave -radix 16 -expand -group {ECC regfile} sim:/top/datapath/regfile/rd_data1_o
add wave -radix 16 -expand -group {ECC regfile} sim:/top/datapath/regfile/rd_data2_o
add wave -radix 16 -expand -group {ECC regfile} sim:/top/datapath/regfile/rd_en1_i
add wave -radix 16 -expand -group {ECC regfile} sim:/top/datapath/regfile/rd_en2_i
add wave -radix 16 -expand -group {ECC regfile} sim:/top/datapath/regfile/ready_o
add wave -radix 16 -expand -group {ECC regfile} sim:/top/datapath/regfile/reg1_double_error_o
add wave -radix 16 -expand -group {ECC regfile} sim:/top/datapath/regfile/reg2_double_error_o
add wave -radix 16 -expand -group {ECC regfile} sim:/top/datapath/regfile/state
add wave -radix 16 -expand -group {ECC regfile} sim:/top/datapath/regfile/reg_eccs
add wave -radix 16 -expand -group {ECC regfile} sim:/top/datapath/regfile/correction_reg_1
add wave -radix 16 -expand -group {ECC regfile} sim:/top/datapath/regfile/correction_reg_2
add wave -radix 16 -expand -group {ECC regfile} sim:/top/datapath/regfile/reg1_read_with_errors
add wave -radix 16 -expand -group {ECC regfile} sim:/top/datapath/regfile/reg2_read_with_errors

add wave -radix 16 -expand -group {Base regfile} sim:/top/datapath/regfile/base_regfile/reset_i
add wave -radix 16 -expand -group {Base regfile} sim:/top/datapath/regfile/base_regfile/rd_addr1_i
add wave -radix 16 -expand -group {Base regfile} sim:/top/datapath/regfile/base_regfile/rd_addr2_i
add wave -radix 16 -expand -group {Base regfile} sim:/top/datapath/regfile/base_regfile/wr_addr_i
add wave -radix 16 -expand -group {Base regfile} sim:/top/datapath/regfile/base_regfile/wr_data_i
add wave -radix 16 -expand -group {Base regfile} sim:/top/datapath/regfile/base_regfile/wr_en_i
add wave -radix 16 -expand -group {Base regfile} sim:/top/datapath/regfile/base_regfile/rd_data1_o
add wave -radix 16 -expand -group {Base regfile} sim:/top/datapath/regfile/base_regfile/rd_data2_o
add wave -radix 16 -expand -group {Base regfile} sim:/top/datapath/regfile/base_regfile/registers
