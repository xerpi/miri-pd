force -freeze sim:/top/clk_i 1 0, 0 {50 ps} -r 100
force -freeze sim:/top/reset_i 1 0, 0 {100 ps} -freeze

add wave -radix 16 sim:/top/reset_i
add wave -radix 16 sim:/top/clk_i

add wave -radix 16 -expand -group {Datapath} sim:/top/datapath/pc
add wave -radix 16 -expand -group {Datapath} sim:/top/datapath/id_reg
add wave -radix 16 -expand -group {Datapath} sim:/top/datapath/ex_reg
add wave -radix 16 -expand -group {Datapath} sim:/top/datapath/mem_reg
add wave -radix 16 -expand -group {Datapath} sim:/top/datapath/wb_reg
add wave -radix 16 -expand -group {Datapath} sim:/top/datapath/regfile/registers

add wave -radix 16 -expand -group {Control} sim:/top/datapath/control_unit/icache_busy
add wave -radix 16 -expand -group {Control} sim:/top/datapath/control_unit/dcache_busy

add wave -radix 16 -expand -group {ICache bus} sim:/top/icache_bus.addr
add wave -radix 16 -expand -group {ICache bus} sim:/top/icache_bus.rd_data
add wave -radix 16 -expand -group {ICache bus} sim:/top/icache_bus.access
add wave -radix 16 -expand -group {ICache bus} sim:/top/icache_bus.hit

add wave -radix 16 -expand -group {DCache bus} sim:/top/dcache_bus.addr
add wave -radix 16 -expand -group {DCache bus} sim:/top/dcache_bus.rd_data
add wave -radix 16 -expand -group {DCache bus} sim:/top/dcache_bus.wr_data
add wave -radix 16 -expand -group {DCache bus} sim:/top/dcache_bus.wr_size
add wave -radix 16 -expand -group {DCache bus} sim:/top/dcache_bus.write
add wave -radix 16 -expand -group {DCache bus} sim:/top/dcache_bus.access
add wave -radix 16 -expand -group {DCache bus} sim:/top/dcache_bus.hit

add wave -radix 16 -expand -group {ECC ICache} sim:/top/icache/state
add wave -radix 16 -expand -group {ECC ICache} sim:/top/icache/load_hit_with_errors
add wave -radix 16 -expand -group {ECC ICache} sim:/top/icache/cur_word_has_errors
add wave -radix 16 -expand -group {ECC ICache} sim:/top/icache/cur_line
add wave -radix 16 -expand -group {ECC ICache} sim:/top/icache/cpu_addr_word
add wave -radix 16 -expand -group {ECC ICache} sim:/top/icache/cpu_addr_woff
add wave -radix 16 -expand -group {ECC ICache} sim:/top/icache/hamming_dec_din
add wave -radix 16 -expand -group {ECC ICache} sim:/top/icache/hamming_dec_parity
add wave -radix 16 -expand -group {ECC ICache} sim:/top/icache/hamming_dec_dout
add wave -radix 16 -expand -group {ECC ICache} sim:/top/icache/hamming_dec_data_error
add wave -radix 16 -expand -group {ECC ICache} sim:/top/icache/lines

add wave -radix 16 -expand -group {ECC DCache} sim:/top/dcache/hamming_dec_double_error
add wave -radix 16 -expand -group {ECC DCache} sim:/top/dcache/double_error
add wave -radix 16 -expand -group {ECC DCache} sim:/top/dcache/state
add wave -radix 16 -expand -group {ECC DCache} sim:/top/dcache/load_hit_with_errors
add wave -radix 16 -expand -group {ECC DCache} sim:/top/dcache/cur_word_has_errors
add wave -radix 16 -expand -group {ECC DCache} sim:/top/dcache/cpu_addr
add wave -radix 16 -expand -group {ECC DCache} sim:/top/dcache/cpu_addr_word
add wave -radix 16 -expand -group {ECC DCache} sim:/top/dcache/cpu_addr_woff
add wave -radix 16 -expand -group {ECC DCache} sim:/top/dcache/cur_line
add wave -radix 16 -expand -group {ECC DCache} sim:/top/dcache/ecc_line_wr_mask
add wave -radix 16 -expand -group {ECC DCache} sim:/top/dcache/hamming_enc_data
add wave -radix 16 -expand -group {ECC DCache} sim:/top/dcache/hamming_enc_parity
add wave -radix 16 -expand -group {ECC DCache} sim:/top/dcache/hamming_dec_din
add wave -radix 16 -expand -group {ECC DCache} sim:/top/dcache/hamming_dec_parity
add wave -radix 16 -expand -group {ECC DCache} sim:/top/dcache/hamming_dec_dout
add wave -radix 16 -expand -group {ECC DCache} sim:/top/dcache/hamming_dec_data_error
add wave -radix 16 -expand -group {ECC DCache} sim:/top/dcache/lines

add wave -radix 16 -expand -group {Base ICache} sim:/top/icache/base_cache/state
add wave -radix 16 -expand -group {Base ICache} sim:/top/icache/base_cache/hit
add wave -radix 16 -expand -group {Base ICache} sim:/top/icache/base_cache/lines

add wave -radix 16 -expand -group {Base DCache} sim:/top/dcache/base_cache/state
add wave -radix 16 -expand -group {Base DCache} sim:/top/dcache/base_cache/hit
add wave -radix 16 -expand -group {Base DCache} sim:/top/dcache/base_cache/lines

add wave -radix 16 -expand -group {Memory arbiter} sim:/top/memory_arbiter/state

add wave -radix 16 -expand -group {ICache memory bus} sim:/top/icache_memory_bus.addr
add wave -radix 16 -expand -group {ICache memory bus} sim:/top/icache_memory_bus.rd_data
add wave -radix 16 -expand -group {ICache memory bus} sim:/top/icache_memory_bus.wr_data
add wave -radix 16 -expand -group {ICache memory bus} sim:/top/icache_memory_bus.write
add wave -radix 16 -expand -group {ICache memory bus} sim:/top/icache_memory_bus.valid
add wave -radix 16 -expand -group {ICache memory bus} sim:/top/icache_memory_bus.ready

add wave -radix 16 -expand -group {DCache memory bus} sim:/top/dcache_memory_bus.addr
add wave -radix 16 -expand -group {DCache memory bus} sim:/top/dcache_memory_bus.rd_data
add wave -radix 16 -expand -group {DCache memory bus} sim:/top/dcache_memory_bus.wr_data
add wave -radix 16 -expand -group {DCache memory bus} sim:/top/dcache_memory_bus.write
add wave -radix 16 -expand -group {DCache memory bus} sim:/top/dcache_memory_bus.valid
add wave -radix 16 -expand -group {DCache memory bus} sim:/top/dcache_memory_bus.ready

add wave -radix 16 -expand -group {Memory bus} sim:/top/memory_bus.addr
add wave -radix 16 -expand -group {Memory bus} sim:/top/memory_bus.rd_data
add wave -radix 16 -expand -group {Memory bus} sim:/top/memory_bus.wr_data
add wave -radix 16 -expand -group {Memory bus} sim:/top/memory_bus.write
add wave -radix 16 -expand -group {Memory bus} sim:/top/memory_bus.valid
add wave -radix 16 -expand -group {Memory bus} sim:/top/memory_bus.ready
